
#ifndef _zf_driver_lpuart_h_
#define _zf_driver_lpuart_h_

#include "hal_uart.h"

#include "zf_common_typedef.h"
#include "zf_driver_dma.h"
#include "hal_lpuart.h"

typedef enum                                                                    // 枚举串口引脚 此枚举定义不允许用户修改
{
    LPUART1_TX_C1=0x041D0,                                               // 0x 041[C1]  D[AF13] 0[UART1]
    
}lpuart_tx_pin_enum;

typedef enum                                                                    // 枚举串口引脚 此枚举定义不允许用户修改
{
    LPUART1_RX_C0=0x040D0,                                               // 0x 040[C0]  D[AF13] 0[UART1]
}lpuart_rx_pin_enum;

typedef enum                                                                    // 枚举串口号 此枚举定义不允许用户修改
{
    LPUART_1,
}lpuart_index_enum;

void lpuart_init(lpuart_index_enum lpuartn,LPUART_Baudrate_Type baud,lpuart_tx_pin_enum tx_pin,lpuart_rx_pin_enum rx_pin);

void lpuart_rx_dma_init(lpuart_index_enum lpuart_n,uint8 priority);
void lpuart_rx_dma_start(lpuart_index_enum lpuart_n,uint8*buf,uint32 len);
void lpuart_rx_dma_stop(lpuart_index_enum lpuart_n);
void lpuart_tx_dma_init(lpuart_index_enum lpuart_n,uint8 priority);
void lpuart_tx_dma_start(lpuart_index_enum lpuart_n,const uint8*buf,uint32 len);
void lpuart_tx_dma_stop(lpuart_index_enum lpuart_n);

extern dma_channel_enum lpuart_rx_dma[1];

extern dma_channel_enum lpuart_tx_dma[1];

#endif
